# Neighborhood Map Udacity Project - Pubs in Burton

This is a project that I created showing some of the pubs in Burton-upon-Trent on an interactive google map and displaying information acquired from Foursquare. To complete this project I used Google's Material Design template, Knockout.js and Foursquare API through AJAX request.

## How to run

To see my app in action click [here](https://davidteoran.github.io/neighborhood-map/)!

## Credits and resources

* http://knockoutjs.com/documentation/introduction.html
* https://getmdl.io/templates/index.html
* https://github.com/lacyjpr/neighborhood
* https://developers.google.com/maps/documentation/javascript/
