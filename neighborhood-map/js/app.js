/******* Model *******/
// These are the pub listings that will be shown to the user
var locations = [{
    name: 'Albion',
    lat: 52.809099,
    lng: -1.662771,
    id: '4c52ce1ad797e21e2ea7147e'
}, {
    name: 'The Coopers Tavern',
    lat: 52.804592,
    lng: -1.637588,
    id: '4c1e4b17b306c9288aca66b7'
}, {
    name: 'Devonshire Arms',
    lat: 52.805399,
    lng: -1.637985,
    id: '4bdd3028be5120a1586efe70'
}, {
    name: 'The Alfred',
    lat: 52.80953,
    lng: -1.641132,
    id: '4b576d0bf964a520063728e3'
}, {
    name: 'The Roebuck Inn',
    lat: 52.805914,
    lng: -1.639553,
    id: '4c7910c181bca09320fafb14'
}, {
    name: 'The Old Cottage Tavern',
    lat: 52.809256,
    lng: -1.645028,
    id: '4c77efe393ef236ab57daa0f'
}, {
    name: 'The Lord Burton',
    lat: 52.80297,
    lng: -1.629462,
    id: '4ba38b84f964a520164638e3'
}, {
    name: 'The Brewery Tap',
    lat: 52.807696,
    lng: -1.631596,
    id: '4db0557afa8ca4b3e9e1097a'
}, {
    name: 'The Corner House',
    lat: 52.795243,
    lng: -1.669166,
    id: '4bbf6e73b083a593fd6ca3e9'
}, {
    name: 'The Leopard',
    lat: 52.857819,
    lng: -1.687904,
    id: '4cfe68d3d7206ea83c3d4e69'
}];

// Initialize the map
var map;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 52.804028,
                lng: -1.647100
            },
            zoom: 14,
            mapTypeControl: false,
            streetViewControl: false
        },
        clearTimeout(googleRequestTimeout)
    );
    // Start the ViewModel here so it doesn't initialize before Google Maps loads
    ko.applyBindings(new ViewModel());
}

var googleRequestTimeout = setTimeout(function() {
    document.getElementById('map').innerHTML = '<h2>Google Maps is not loading. Please try refreshing the page later.</h2>';
}, 8000);

//Create new instances of pubs when called
var Pub = function(data) {
    this.name = data.name;
    this.lat = data.lat;
    this.lng = data.lng;
    this.id = data.id;
    this.marker = ko.observable();
    this.phone = ko.observable('');
    this.description = ko.observable('');
    this.address = ko.observable('');
    this.rating = ko.observable('');
    this.url = ko.observable('');
    this.canonicalUrl = ko.observable('');
    this.photoPrefix = ko.observable('');
    this.photoSuffix = ko.observable('');
    this.contentString = ko.observable('');
};


/******* ViewModel *******/

var ViewModel = function() {
    // Make this accessible
    var self = this;

    // Create an array of all pubs
    this.pubList = ko.observableArray([]);

    // Call the Pub constructor
    // Create Pub objects for each item in locations and store them in the above array
    locations.forEach(function(pubItem) {
        self.pubList.push(new Pub(pubItem));
    });

    // Initialize the infowindow
    var infowindow = new google.maps.InfoWindow({
        maxWidth: 300,
    });

    // Initialize marker
    var marker;

    // For each place, set markers, request Foursquare data, and set event listeners for the infowindow
    self.pubList().forEach(function(pubItem) {
        var contentString;

        // Define markers for each place
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(pubItem.lat, pubItem.lng),
            map: map,
            animation: google.maps.Animation.DROP
        });
        pubItem.marker = marker;

        // Make AJAX request to Foursquare
        $.ajax({
            url: 'https://api.foursquare.com/v2/venues/' + pubItem.id +
                '?client_id=ZXHJ4NCIYVF3ZBYSQNY5O22LKGGBR1SVC3MGQEKVYT0BQ20H&client_secret=OW11DQMKGPTEHWA4TVBFUYOBDA134RW3BNJ1402ENOUKL5U5&v=20130815',
            dataType: 'json'
        }).done(function(data) {
                // Make results easier to handle
                var result = data.response.venue;


                // The following lines handle inconsistent results from Foursquare
                // Check each result for properties, if the property exists,
                // add it to the Place constructor
                var contact = result.hasOwnProperty('contact') ? result.contact : '';
                if (contact.hasOwnProperty('formattedPhone')) {
                    pubItem.phone(contact.formattedPhone || '');
                }

                var location = result.hasOwnProperty('location') ? result.location : '';
                if (location.hasOwnProperty('address')) {
                    pubItem.address(location.address || '');
                }

                var bestPhoto = result.hasOwnProperty('bestPhoto') ? result.bestPhoto : '';
                if (bestPhoto.hasOwnProperty('prefix')) {
                    pubItem.photoPrefix(bestPhoto.prefix || '');
                }

                if (bestPhoto.hasOwnProperty('suffix')) {
                    pubItem.photoSuffix(bestPhoto.suffix || '');
                }

                var description = result.hasOwnProperty('description') ? result.description : '';
                pubItem.description(description || '');

                var rating = result.hasOwnProperty('rating') ? result.rating : '';
                pubItem.rating(rating || 'none');

                var url = result.hasOwnProperty('url') ? result.url : '';
                pubItem.url(url || '');

                pubItem.canonicalUrl(result.canonicalUrl);

                // Infowindow code is in the success function so that the error message
                // displayed in infowindow works properly, instead of a mangled infowindow

                // Content of the infowindow
                contentString = '<div id="iWindow"><h4>' + pubItem.name + '</h4><div id="pic"><img src="' +
                    pubItem.photoPrefix() + '200x150' + pubItem.photoSuffix() +
                    '" alt="Image Location"></div><p>Information from Foursquare:</p><p>' +
                    pubItem.phone() + '</p><p>' + pubItem.address() + '</p><p>' +
                    pubItem.description() + '</p><p>Rating: ' + pubItem.rating() +
                    '</p><p><a href=' + pubItem.url() + '>' + pubItem.url() +
                    '</a></p><p><a target="_blank" href=' + pubItem.canonicalUrl() +
                    '>Foursquare Page</a></p><p><a target="_blank" href=https://www.google.com/maps/dir/Current+Location/' +
                    pubItem.lat + ',' + pubItem.lng + '>Directions</a></p></div>';


            }).fail(function(e) {
            // Alert the user on error. Set messages in the DOM and infowindow
                infowindow.setContent('<h5>Foursquare data is unavailable. Please try refreshing later.</h5>');
            }).always(function() {
                google.maps.event.addListener(pubItem.marker, 'click', function() {
                    infowindow.open(map, this);
                    map.panTo(pubItem.marker.position);
                    pubItem.marker.setAnimation(google.maps.Animation.BOUNCE);
                    setTimeout(function() {
                        pubItem.marker.setAnimation(null);
                    }, 700);
                    infowindow.setContent(contentString);
                });
        });
    });

    // Activate the appropriate marker when the user clicks a list item
    self.showInfo = function(pubItem) {
        google.maps.event.trigger(pubItem.marker, 'click');
    };

    // Filter markers per user input
    // Array containing only the markers based on filter
    self.visible = ko.observableArray();

    // All markers are visible by default before any user input
    self.pubList().forEach(function(pub) {
        self.visible.push(pub);
    });

    // Track filter input
    self.filterInput = ko.observable('');

    // If filter input is included in the pub name, make it and its marker visible
    // Otherwise, remove the pub and marker
    self.filterMarkers = function() {
        // Set all markers and pubs to not visible.
        var filterInput = self.filterInput().toLowerCase();
        self.visible.removeAll();
        self.pubList().forEach(function(pub) {
            pub.marker.setVisible(false);
            // Compare the name of each place to filter input
            // If filter input is included in the name, set the pub and marker as visible
            if (pub.name.toLowerCase().indexOf(filterInput) !== -1) {
                self.visible.push(pub);
            }
        });
        self.visible().forEach(function(pub) {
            pub.marker.setVisible(true);
        });
    };

    //Variable for storing checkbox value
    self.toggleListings = ko.observable(true);

    //This funtion runs when the toggle is clicked to show or hide markers
    self.toggle = function() {
        if (self.toggleListings()) {
            var bounds = new google.maps.LatLngBounds();
            // Extend the boundaries of the map for each marker and display the marker
            self.visible().forEach(function(pub) {
                    pub.marker.setVisible(true);
                    bounds.extend(pub.marker.position);
                }),
                map.fitBounds(bounds);
        } else {
            self.visible().forEach(function(pub) {
                pub.marker.setVisible(false);
            });
        }
        return true;
    };

}; // ViewModel
