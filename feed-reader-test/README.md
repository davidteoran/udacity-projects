# Udacity Feed Reader Test project

## Overview

In this project, I learned about testing with Javascript. Testing is an important part of the development process and many organizations practice a standard known as "test-driven development" or TDD. This is when developers write tests first, before they ever start developing their application.

I completed this project using the Jasmine framework and the Red, Green, Refactor method:
* Red: Create a test and make it fail,
* Green: Make the test pass by any means necessary,
* Refactor: Change the code to remove duplication in your project and to improve the design while ensuring that all tests still pass.

## Quickstart

To run:

1. Download or fork the application from:

https://github.com/davidteoran/udacity-projects/tree/gh-pages/feed-reader-test

2. Select index.html to start the app

or:

Click [here](https://davidteoran.github.io/udacity-projects/feed-reader-test) to view the project result.


## Credits and Resources

* http://jasmine.github.io/2.1/introduction.html
* https://github.com/walesmd/udacity-frontend-feedreader/blob/master/jasmine/spec/feedreader-solution.js
