# Puppy Clicker Application

## Welcome

This project is a clone from Udacity's Cat Clikcer application. The purpose of this project was to learn how to use Knockout JS while building a relatively simple app.

## To Start

Please follow the [link](https://davidteoran.github.io/puppy-clicker/)!

## My Contribution

I wrote the app.js code and used Bootstrap framework to style the app.
