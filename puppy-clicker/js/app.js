/******* Model *******/
var initialPuppies = [
        {
            clickCount : 0,
            name : 'Tabby',
            imgSrc : 'img/puppy.jpeg',
            nickname: ['Trotty']
        },
        {
            clickCount : 0,
            name : 'Tiger',
            imgSrc : 'img/puppy3.jpg',
            nickname: ['Tigry']
        },
        {
            clickCount : 0,
            name : 'Scaredy',
            imgSrc : 'img/puppy2.jpg',
            nickname: ['Caspy']
        },
        {
            clickCount : 0,
            name : 'Shadow',
            imgSrc : 'img/puppy1.jpg',
            nickname: ['Black']
        },
        {
            clickCount : 0,
            name : 'Plup',
            imgSrc : 'img/puppy4.jpg',
            nickname: ['Pluppy']
        }
];

//Create new instances of puppy when called
var Puppy = function(data) {
this.clickCount = ko.observable(data.clickCount);
this.name = ko.observable(data.name);
this.imgSrc = ko.observable(data.imgSrc);
this.imgAttribution = ko.observable(data.imgAttribution);

//Change levels on when click increments
this.title = ko.computed(function() {
  var title;
  var clicks = this.clickCount();
  if(clicks < 50) {
    return title = 'Novice';
  } else if(clicks < 100) {
    return title = 'Apprentice';
  } else if(clicks < 1000) {
    return title = "Master";
  } else {
    return title = 'To infinity and beyond!';
  }

  return title;
},this);

this.nickname = ko.observableArray(data.nickname);

}

/******* ViewModel *******/
var ViewModel = function() {
    //Create variable 'that' which refers to ViewModel
    var that = this;

    //Declare array for list of puppies
    this.puppyList = ko.observableArray([]);

    //Create new puppy from the initial puppy array
    initialPuppies.forEach(function(puppyItem) {
      that.puppyList.push(new Puppy(puppyItem));
    });

    //Set current puppy to the first one from the array
    this.currentPuppy = ko.observable(this.puppyList()[0]);

    //Display selected puppy on click
    this.selectedPuppy = function(clickedPuppy) {
      that.currentPuppy(clickedPuppy);
    };

    //Increment clicks for each puppy separately
    this.incrementCounter = function() {
      this.clickCount(this.clickCount() + 1);
    };
}

//Activate ViewModel
ko.applyBindings(new ViewModel());
