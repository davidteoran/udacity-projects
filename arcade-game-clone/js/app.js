//Declare global variables and set possible X and Y coordinates
var ENEMY_SPEED = 150,
    COLLISION_AREA = 50,
    TILE_WIDTH = 100,
    TILE_HEIGHT = 83,
    coorX = [0, 100, 200, 300, 400],
    coorY = [60, 143, 226];

// Enemies our player must avoid
var Enemy = function() {
    // Variables applied to each of our instances go here,
    // we've provided one for you to get started

    // The image/sprite for our enemies, this uses
    // a helper we've provided to easily load images
    this.sprite = 'images/enemy-bug.png';
    this.x = -100;
    this.y = coorY[Math.floor(Math.random() * 3)];
    this.speed = Math.floor((Math.random() * ENEMY_SPEED) + 65);
};

// Update the enemy's position, required method for game
// Parameter: dt, a time delta between ticks
Enemy.prototype.update = function(dt) {
    // You should multiply any movement by the dt parameter
    // which will ensure the game runs at the same speed for
    // all computers.
    this.x = this.x + (this.speed * dt);
    if (this.x > 550) {
        this.x = -100;
        this.y = this.y + TILE_HEIGHT;
        this.speed = Math.floor((Math.random() * ENEMY_SPEED) + 65);
    } else if (this.y > 226) {
          this.y = 60;
    }

    //Reset player if collision occurs
    if ((player.y >= this.y - COLLISION_AREA && player.y <= this.y + COLLISION_AREA) && (player.x >= this.x - COLLISION_AREA && player.x <= this.x + COLLISION_AREA)) {
        player.reset();
    }
};

// Draw the enemy on the screen, required method for game
Enemy.prototype.render = function() {
    ctx.drawImage(Resources.get(this.sprite), this.x, this.y);
};


// Now write your own player class
// This class requires an update(), render() and
// a handleInput() method.


// Player class with initial x and y coordinates.
var Player = function () {
    this.x = 200;
    this.y = 400;
};

// Set key functions for player
Player.prototype.update = function () {
    if ((this.control === 'left' || this.control === 'a') && this.x > 0) {
        this.x = this.x - TILE_WIDTH;
    } else if ((this.control === 'right' || this.control === 'd') && this.x != 400) {
        this.x = this.x + TILE_WIDTH;
    } else if (this.control === 'up' || this.control === 'w') {
        this.y = this.y - TILE_HEIGHT;
    } else if ((this.control === 'down' || this.control === 's') && this.y != 400) {
        this.y = this.y + TILE_HEIGHT;
    }
    this.control = null;
    if (this.y < 60){
        this.reset();
    }
};

// Draw the player on the screen!
Player.prototype.render = function () {
    ctx.drawImage(Resources.get('images/char-boy.png'), this.x, this.y);
};

// Set the key
Player.prototype.handleInput = function (key) {
    this.control = key;
};

// On reset, put player back in initial position
Player.prototype.reset = function () {
    this.x = 200;
    this.y = 400;
};

// Now instantiate your objects.
// Place all enemy objects in an array called allEnemies
var allEnemies = [new Enemy(), new Enemy(), new Enemy(), new Enemy()];

// Place the player object in a variable called player
var player = new Player();

// This listens for key presses and sends the keys to your
// Player.handleInput() method. You don't need to modify this.
document.addEventListener('keyup', function(e) {
    var allowedKeys = {
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down',
        65: 'a',
        87: 'w',
        68: 'd',
        83: 's'
    };

    player.handleInput(allowedKeys[e.keyCode]);
});
