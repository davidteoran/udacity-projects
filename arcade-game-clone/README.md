Udacity project - Frontend Nanodegree Arcade Game
===============================

# Start the game

You can start the game by clicking on the following link: [Let's play!](https://davidteoran.github.io/arcade-game-clone/)

# How to play

Move you character with the arrow keys or with w, a, s, d

Avoid enemies(bugs) as you try to make your way to the water

Once you reach water your hero will respawn and you can begin a new game

# Sources

- http://www.w3schools.com/js/js_object_prototypes.asp
- https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Statements/if...else
- [JavaScript & jQuery by Jon Duckett](http://javascriptbook.com/)

# More features to come

- collectable items
- character selection
- bigger playing area
- opening page, game over and win page

# License

MIT License

Copyright (c) 2016 David Teoran

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
