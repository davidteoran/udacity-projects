"use strict";

var bio = {
    "name": "David Teoran",
    "role": "Junior Web Developer",
    "contacts": {
        "mobile": "07447-803-398",
        "email": "davidteoran@gmail.com",
        "github": "davidteoran",
        "location": "Burton-upon-Trent, United Kingdom"
    },
    "welcomeMessage": "Junior Web Developer looking for a challenging opportunity!",
    "skills": ["HTML5", "CSS3", "Javascript", "GitHub", "Bootstrap", "jQuery"],
    "biopic": "images/me.jpg",
    "display": function() {
        var formattedRole = HTMLheaderRole.replace("%data%", bio.role),
            formattedName = HTMLheaderName.replace("%data%", bio.name),
            formattedBioPic = HTMLbioPic.replace("%data%", bio.biopic);

        $("#header").prepend(formattedRole).prepend(formattedName).prepend(formattedBioPic);

        for (var contact in bio.contacts) {
            if (bio.contacts.hasOwnProperty(contact)) {
                var formattedContactInfo = HTMLcontactGeneric.replace("%contact%", contact).replace("%data%", bio.contacts[contact]);

                $("#topContacts").append(formattedContactInfo);

            }
        }

        var mobile = $("<li></li>").text(bio.contacts.mobile),
            email = $("<li></li>").text(bio.contacts.email),
            github = $("<li></li>").text(bio.contacts.github);

        $("#footerContacts").append(mobile, email, github);


        var formattedMessage = HTMLwelcomeMsg.replace("%data%", bio.welcomeMessage);
        $("#header").append(formattedMessage);

        if (bio.skills.length > 0) {
            $("#header").append(HTMLskillsStart);

            bio.skills.forEach(function(skill) {
                var formattedSkill = HTMLskills.replace("%data%", skill);
                $("#skills").append(formattedSkill);
            });
        }
    }

};

var education = {
    "schools": [{
        "name": "Eötvös Loránd University",
        "location": "Budapest, Hungary",
        "degree": "B.Sc.",
        "majors": ["Earth Sciences"],
        "dates": "2012 - 2016",
        "url": "http://ttk.elte.hu/"
    }],

    "onlineCourses": [{
        "title": "Front-End Web Developer Nanodegree",
        "school": "Udacity",
        "date": "In Progress",
        "url": "https://www.udacity.com/course/front-end-web-developer-nanodegree--nd001"
    }, {
        "title": "JavaScript Basics",
        "school": "SoloLearn",
        "date": "2016",
        "url": "http://www.sololearn.com/Course/JavaScript/"
    }],
    "display": function() {
        education.schools.forEach(function(school) {
            $("#education").append(HTMLschoolStart);

            var formattedSchoolName = HTMLschoolName.replace("%data%", school.name).replace("#", school.url),
                formattedDegree = HTMLschoolDegree.replace("%data%", school.degree),
                formattedDates = HTMLschoolDates.replace("%data%", school.dates),
                formattedLocation = HTMLschoolLocation.replace("%data%", school.location);

            $(".education-entry:last").append(formattedSchoolName + formattedDegree).append(formattedDates).append(formattedLocation);

            school.majors.forEach(function(major) {
                var formattedMajor = HTMLschoolMajor.replace("%data%", major);
                $(".education-entry:last").append(formattedMajor);
            });

        });

        $("#education").append(HTMLonlineClasses);
        education.onlineCourses.forEach(function(course) {
            $("#education").append(HTMLschoolStart);

            var formattedOnlineTitle = HTMLonlineTitle.replace("%data%", course.title);
            var formattedOnlineSchool = HTMLonlineSchool.replace("%data%", course.school);
            var formattedOnlineDate = HTMLonlineDates.replace("%data%", course.date);
            var formattedOnlineURL = HTMLonlineURL.replace("%data%", course.url);

            $(".education-entry:last").append(formattedOnlineTitle + formattedOnlineSchool).append(formattedOnlineDate).append(formattedOnlineURL);
        });

    }
};

var work = {
    "jobs": [{
        "employer": "Geogold Kárpátia Enviromental Consulting Ltd.",
        "title": "Digitizer",
        "location": "Budapest, Hungary",
        "dates": "AUG 2015 - SEP 2015",
        "description": "Digitalised old paper well logs with industry-standard software Neuralog and simplified working process with logs."
    }],
    "display": function() {
        for (var job in work.jobs) {
            if (work.jobs.hasOwnProperty(job)) {
                $("#workExperience").append(HTMLworkStart);

                var formattedEmployer = HTMLworkEmployer.replace("%data%", work.jobs[job].employer),
                    formattedTitle = HTMLworkTitle.replace("%data%", work.jobs[job].title),
                    formattedEmployerTitle = formattedEmployer + formattedTitle,
                    formattedDates = HTMLworkDates.replace("%data%", work.jobs[job].dates),
                    formattedDescription = HTMLworkDescription.replace("%data%", work.jobs[job].description),
                    formattedWorkLocation = HTMLworkLocation.replace("%data%", work.jobs[job].location);

                $(".work-entry:last").append(formattedEmployerTitle).append(formattedDates).append(formattedWorkLocation).append(formattedDescription);
            }
        }
    }
};

var projects = {
    "projects": [{
        "title": "Portfolio Site",
        "dates": "2016",
        "description": "Built a responsive portfolio site as a Udacity project.",
        "images": [
            "images/project1.jpg"
        ]
    }],
    "display": function() {
        for (var project in projects.projects) {
            if (projects.projects.hasOwnProperty(project)) {
                $("#projects").append(HTMLprojectStart);

                var formattedTitle = HTMLprojectTitle.replace("%data%", projects.projects[project].title),
                    formattedDates = HTMLprojectDates.replace("%data%", projects.projects[project].dates),
                    formattedDescription = HTMLprojectDescription.replace("%data%", projects.projects[project].description);

                $(".project-entry:last").append(formattedTitle).append(formattedDates).append(formattedDescription);

                if (projects.projects[project].images.length > 0) {
                    projects.projects[project].images.forEach(function(image) {
                          var formattedImage = HTMLprojectImage.replace("%data%", image);

                          $(".project-entry:last").append(formattedImage);
                      });

                }
            }
        }
    }
};

bio.display();
education.display();
work.display();
projects.display();

function locationizer(work_obj) {
    var locations = [];
    for (var job in work_obj.jobs) {
        if (work_obj.jobs.hasOwnProperty(job)) {
            locations.push(work_obj.jobs[job].location);
        }
    }
    return locations;
}

$("#mapDiv").append(googleMap);
